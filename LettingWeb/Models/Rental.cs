﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LettingWeb.Models
{
    public class Rental
    {
        [Key]
        public int PropertyId { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public int Rent { get; set; }
        public string Photo { get; set; }
    }
}
