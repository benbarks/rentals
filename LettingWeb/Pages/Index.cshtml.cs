﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using LettingWeb.Models;

namespace LettingWeb.Pages
{
    public class IndexModel : PageModel
    {
        private readonly LettingWeb.Models.AppDbContext _context;

        public IndexModel(LettingWeb.Models.AppDbContext context)
        {
            _context = context;
        }

        public IList<Rental> Rental { get;set; }

        public async Task OnGetAsync()
        {
            Rental = await _context.Rental.ToListAsync();
        }
    }
}
