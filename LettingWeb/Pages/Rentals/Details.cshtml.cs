﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using LettingWeb.Models;

namespace LettingWeb.Pages.Rentals
{
    public class DetailsModel : PageModel
    {
        private readonly LettingWeb.Models.AppDbContext _context;

        public DetailsModel(LettingWeb.Models.AppDbContext context)
        {
            _context = context;
        }

        public Rental Rental { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Rental = await _context.Rental.FirstOrDefaultAsync(m => m.PropertyId == id);

            if (Rental == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
